# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2012, 2016, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2021-10-23 22:54+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: main.cpp:26
#, kde-format
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "Information de PCI"

#~ msgid "kcm_pci"
#~ msgstr "kcm_pci"

#~ msgid "PCI Devices"
#~ msgstr "Dispositivo PCI"

#~ msgid "(c) 2008 Nicolas Ternisien(c) 1998 - 2002 Helge Deller"
#~ msgstr "(c) 2008 Nicolas Ternisien(c)(c) 1998-2002 Helge Deller"

#~ msgid "Nicolas Ternisien"
#~ msgstr "Nicolas Ternisien"

#~ msgid "Helge Deller"
#~ msgstr "Helge Deller"

#~ msgid "This list displays PCI information."
#~ msgstr "Iste lista monstra information de PCI."

#~ msgid ""
#~ "This display shows information about your computer's PCI slots and the "
#~ "related connected devices."
#~ msgstr ""
#~ "Iste monstrator monstra information super le slots PCI de tu computator e "
#~ "le dispositivos contingite a illos."

#~ msgid "Device Class"
#~ msgstr "Classe de dispositivo"

#~ msgid "Device Subclass"
#~ msgstr "Subclasse de dispositivo"

#~ msgid "Device Programming Interface"
#~ msgstr "Interfacie de programmation de Dispositivo"

#~ msgid "Master IDE Device"
#~ msgstr "Dispositivo Magistro IDE"

#~ msgid "Secondary programmable indicator"
#~ msgstr "Indicator programmable secundari"

#~ msgid "Secondary operating mode"
#~ msgstr "Modo de operar secundari"

#~ msgid "Primary programmable indicator"
#~ msgstr "Indicator programmable primari"

#~ msgid "Primary operating mode"
#~ msgstr "Modo de operar primari"

#~ msgid "Vendor"
#~ msgstr "Venditor"

#~ msgid "Device"
#~ msgstr "Dispositivo"

#~ msgid "Subsystem"
#~ msgstr "Subsystema"

#~ msgid " - device:"
#~ msgstr " - Dispositivo:"

#~ msgid "Interrupt"
#~ msgstr "Interruption"

#~ msgid "IRQ"
#~ msgstr "IRQ"

#~ msgid "Pin"
#~ msgstr "Pin"

#~ msgid "Control"
#~ msgstr "Controlo"

#~ msgid "Response in I/O space"
#~ msgstr "Responsa in spatio I/E"

#~ msgid "Response in memory space"
#~ msgstr "Responsa in spatio de memoria"

#~ msgid "Bus mastering"
#~ msgstr "Dominar le Bus"

#~ msgid "Response to special cycles"
#~ msgstr "Responsa a cyclos special"

#~ msgid "Memory write and invalidate"
#~ msgstr "Scriptura de memoria e invalidar"

#~ msgid "Palette snooping"
#~ msgstr "Investigation de paletta"

#~ msgid "Parity checking"
#~ msgstr "Verifica de paritate"

#~ msgid "Address/data stepping"
#~ msgstr "Adresse/datos de passo"

#~ msgid "System error"
#~ msgstr "Error de systema"

#~ msgid "Back-to-back writes"
#~ msgstr "Scripturas retro a retro"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Interrupt status"
#~ msgstr "Stato de Interruption"

#~ msgid "Capability list"
#~ msgstr "Lista de capacitate"

#~ msgid "66 MHz PCI 2.1 bus"
#~ msgstr "bus 66Mhz PCI 2.1"

#~ msgid "User-definable features"
#~ msgstr "Characteristicas definibile per usator"

#~ msgid "Accept fast back-to-back"
#~ msgstr "Da acceptation rapide retro a retro"

#~ msgid "Data parity error"
#~ msgstr "Error de paritate de dato"

#~ msgid "Device selection timing"
#~ msgstr "Tempore de selection de dispositivo"

#~ msgid "Signaled target abort"
#~ msgstr "On signalava abortar de objectivo"

#~ msgid "Received target abort"
#~ msgstr "On recipeva abortar de objectivo"

#~ msgid "Received master abort"
#~ msgstr "On recipeva Abortar de maestro"

#~ msgid "Signaled system error"
#~ msgstr "On signalava error de systema"

#~ msgid "Parity error"
#~ msgstr "Error de paritate"

#~ msgid "Latency"
#~ msgstr "Latentia"

#~ msgid "MIN_GNT"
#~ msgstr "MIN_GNT"

#~ msgid "No major requirements (0x00)"
#~ msgstr "Nulle requisitos major (0x00)"

#~ msgid "MAX_LAT"
#~ msgstr "MAX_LAT"

#~ msgid "Header"
#~ msgstr "Capite"

#~ msgid "Type"
#~ msgstr "Typo"

#~ msgid "Multifunctional"
#~ msgstr "Multifunctional"

#~ msgid "Build-in self test"
#~ msgstr "Auto prova interne"

#~ msgid "BIST Capable"
#~ msgstr "Capabile de BIST"

#~ msgid "BIST Start"
#~ msgstr "Initio de BIST"

#~ msgid "Completion code"
#~ msgstr "Codice de completamento"

#~ msgid "Size"
#~ msgstr "Grandor"

#~ msgid "Address mappings"
#~ msgstr "Mappa de adresses"

#~ msgid "Mapping %1"
#~ msgstr "Mappante  %1"

#~ msgid "Space"
#~ msgstr "Spatio"

#~ msgid "I/O"
#~ msgstr "I/E"

#~ msgid "Memory"
#~ msgstr "Memoria"

#~ msgid "Prefetchable"
#~ msgstr "Pre-recuperabile"

#~ msgid "Address"
#~ msgstr "Adresse"

#~ msgctxt "unassigned address"
#~ msgid "Unassigned"
#~ msgstr "Non assignate"

#~ msgctxt "unassigned size"
#~ msgid "Unassigned"
#~ msgstr "Non assignate"

#~ msgid "Bus"
#~ msgstr "Bus"

#~ msgid "Primary bus number"
#~ msgstr "Numero de bus primari"

#~ msgid "Secondary bus number"
#~ msgstr "Numero de bus secundari"

#~ msgid "Subordinate bus number"
#~ msgstr "Numero de bus subordinate"

#~ msgid "Secondary latency timer"
#~ msgstr "Temporisator de latentia secundari"

#~ msgid "CardBus number"
#~ msgstr "Numero de CardBus"

#~ msgid "CardBus latency timer"
#~ msgstr "Temporisator de latentia de CardBus"

#~ msgid "Secondary status"
#~ msgstr "Stato secundari"

#~ msgid "I/O behind bridge"
#~ msgstr "I/E ponte ultra (I/O behind bridge)"

#~ msgid "32-bit"
#~ msgstr "32-bit"

#~ msgid "Base"
#~ msgstr "Base"

#~ msgid "Limit"
#~ msgstr "Limite"

#~ msgid "Memory behind bridge"
#~ msgstr "Memoria ultra ponte"

#~ msgid "Prefetchable memory behind bridge"
#~ msgstr "Memoria pre-recuperabile ultra ponte"

#~ msgid "64-bit"
#~ msgstr "64-bit"

#~ msgid "Bridge control"
#~ msgstr "Controlo de Ponte"

#~ msgid "Secondary parity checking"
#~ msgstr "Verificar paritate secundari"

#~ msgid "Secondary system error"
#~ msgstr "Error de systema secundari"

#~ msgid "ISA ports forwarding"
#~ msgstr "invio de portos ISA"

#~ msgid "VGA forwarding"
#~ msgstr "invio de VGA"

#~ msgid "Master abort"
#~ msgstr "Abortar Maestro"

#~ msgid "Secondary bus reset"
#~ msgstr "Reinitialisar  bus secundari"

#~ msgid "Secondary back-to-back writes"
#~ msgstr "Scripturas retro a retro secundari"

#~ msgid "Primary discard timer counts"
#~ msgstr "Computos de temporisator pro abandonar Primari"

#~ msgid "2e10 PCI clocks"
#~ msgstr "2e10 horologios PCI (clocks)"

#~ msgid "2e15 PCI clocks"
#~ msgstr "2e15 horologios PCI (clocks)"

#~ msgid "Secondary discard timer counts"
#~ msgstr "Computos de temporisator pro abandonar Secundari"

#~ msgid "Discard timer error"
#~ msgstr "Error de temporisator de abandono"

#~ msgid "Discard timer system error"
#~ msgstr "Error de systema de temporisator de abandono"

#~ msgid "Expansion ROM"
#~ msgstr "Expansion ROM"

#~ msgid "Memory windows"
#~ msgstr "Fenestras de memoria"

#~ msgid "Window %1"
#~ msgstr "Fenestra %1"

#~ msgid "I/O windows"
#~ msgstr "Fenestras de I/E"

#~ msgid "16-bit"
#~ msgstr "16-bit"

#~ msgid "16-bit legacy interface ports"
#~ msgstr "Portos de interfacie de hereditate 16-bit"

#~ msgid "CardBus control"
#~ msgstr "Controlo de CardBus"

#~ msgid "Interrupts for 16-bit cards"
#~ msgstr "Interruptiones pro cartas 16-bit"

#~ msgid "Window 0 prefetchable memory"
#~ msgstr "Memoria pre-recuperabile Fenestra 0"

#~ msgid "Window 1 prefetchable memory"
#~ msgstr "Memoria pre-recuperabile Fenestra 1"

#~ msgid "Post writes"
#~ msgstr "Post Scripturas"

#~ msgid "Raw PCI config space"
#~ msgstr "Spatio de config de PCI Crude"

#~ msgid "Capabilities"
#~ msgstr "Capabilitates"

#~ msgid "Version"
#~ msgstr "Version"

#~ msgid "Clock required for PME generation"
#~ msgstr "Clock requirite pro generation de PME"

#~ msgid "Device-specific initialization required"
#~ msgstr "Initialisation specific de dispositivo requirite "

#~ msgid "Maximum auxiliary current required in D3 cold"
#~ msgstr "Maxime currente auxiliar requirite in D3 frigide"

#~ msgid "D1 support"
#~ msgstr "Supporto de D1"

#~ msgid "D2 support"
#~ msgstr "Supporto de D2"

#~ msgid "Power management events"
#~ msgstr "Eventos de gestion de energia"

#~ msgid "D0"
#~ msgstr "D0"

#~ msgid "D1"
#~ msgstr "D1"

#~ msgid "D2"
#~ msgstr "D2"

#~ msgid "D3 hot"
#~ msgstr "D3 calide"

#~ msgid "D3 cold"
#~ msgstr "D3 frigide"

#~ msgid "Power state"
#~ msgstr "Stato de alimentation"

#~ msgid "Power management"
#~ msgstr "Gestion de Energia"

#~ msgid "Data select"
#~ msgstr "Selige datos"

#~ msgid "Data scale"
#~ msgstr "Scala de datos"

#~ msgid "Power management status"
#~ msgstr "Stato de gestion de energia"

#~ msgid "Bridge status"
#~ msgstr "Stato de ponte"

#~ msgid "Secondary bus state in D3 hot"
#~ msgstr "Stato de bus secundari in D3 calide"

#~ msgid "B2"
#~ msgstr "B2"

#~ msgid "B3"
#~ msgstr "B3"

#~ msgid "Secondary bus power & clock control"
#~ msgstr "Controlo de bus secundari de clock & alimentation"

#~ msgid "Data"
#~ msgstr "Datos"

#~ msgid "Revision"
#~ msgstr "Revision"

#~ msgid "Rate"
#~ msgstr "Frequentia"

#~ msgid "AGP 3.0 mode"
#~ msgstr "modo AGP 3.0"

#~ msgid "Fast Writes"
#~ msgstr "Scripturas rapide"

#~ msgid "Address over 4 GiB"
#~ msgstr "Adresse supra 4 GiB"

#~ msgid "Translation of host processor access"
#~ msgstr "Translation de accesso de processor calide"

#~ msgid "64-bit GART"
#~ msgstr "64-bit GART"

#~ msgid "Cache Coherency"
#~ msgstr "Coherentia de cache"

#~ msgid "Side-band addressing"
#~ msgstr "Adressar de banda lateral (side-band addressing)"

#~ msgid "Calibrating cycle"
#~ msgstr "Cyclo de calibrar"

#~ msgid "Optimum asynchronous request size"
#~ msgstr "Dimension de requesta asynchron optime"

#~ msgid "Isochronous transactions"
#~ msgstr "Transactiones Isochron"

#~ msgid "Maximum number of AGP command"
#~ msgstr "Maxime numero de commando AGP"

#~ msgid "Configuration"
#~ msgstr "Configuration"

#~ msgid "AGP"
#~ msgstr "AGP"

#~ msgid "Data address"
#~ msgstr "Adresse de datos"

#~ msgid "Transfer completed"
#~ msgstr "Transferimento completate"

#~ msgid "Message control"
#~ msgstr "Controlo de message"

#~ msgid "Message signaled interrupts"
#~ msgstr "Interruptiones signalate per messages"

#~ msgid "Multiple message capable"
#~ msgstr "Capacitate de message multiple"

#~ msgid "Multiple message enable"
#~ msgstr "Habilita message multiple"

#~ msgid "64-bit address"
#~ msgstr "Adresse de 64-bit"

#~ msgid "Per vector masking"
#~ msgstr "Mascarar per vector"

#~ msgid "Mask"
#~ msgstr "Mascara"

#~ msgid "Pending"
#~ msgstr "Pendente"

#~ msgid "Length"
#~ msgstr "Longitude"

#~ msgctxt "no data"
#~ msgid "None"
#~ msgstr "Necun"

#~ msgid "Next"
#~ msgstr "Proxime"

#~ msgid "0x00 (None)"
#~ msgstr "0x00 (necun)"

#~ msgid "Root only"
#~ msgstr "Solmente de radice (root o superusator)"

#~ msgid "Value"
#~ msgstr "Valor"

#~ msgid "Cache line size"
#~ msgstr "dimension de linea de cache"

#~ msgctxt "state of PCI item"
#~ msgid "Enabled"
#~ msgstr "Habilitate"

#~ msgctxt "state of PCI item"
#~ msgid "Disabled"
#~ msgstr "Dishabilitate"

#~ msgctxt "state of PCI item"
#~ msgid "Yes"
#~ msgstr "Si"

#~ msgctxt "state of PCI item"
#~ msgid "No"
#~ msgstr "No"

#~ msgctxt "state of PCI item"
#~ msgid "Unknown"
#~ msgstr "Incognite"

#~ msgid "Unclassified device"
#~ msgstr "Dispositivo non classificate"

#~ msgid "Mass storage controller"
#~ msgstr "Controlator de memoria (immagazinage) de massa"

#~ msgid "Network controller"
#~ msgstr "Controlator de rete"

#~ msgid "Display controller"
#~ msgstr "Controlator de schermo"

#~ msgid "Multimedia controller"
#~ msgstr "Controlator de multimedia"

#~ msgid "Memory controller"
#~ msgstr "Controlator de memoria"

#~ msgid "Bridge"
#~ msgstr "Ponte (Bridge)"

#~ msgid "Communication controller"
#~ msgstr "Controlator de communication"

#~ msgid "Generic system peripheral"
#~ msgstr "Systema generic peripheral"

#~ msgid "Input device controller"
#~ msgstr "Controlator de dispositivo de ingresso"

#~ msgid "Docking station"
#~ msgstr "Station de bassino (dock station)"

#~ msgid "Processor"
#~ msgstr "Processor"

#~ msgid "Serial bus controller"
#~ msgstr "Controlator de bus serial"

#~ msgid "Wireless controller"
#~ msgstr "Controlator wireless (sin cablos)"

#~ msgid "Intelligent controller"
#~ msgstr "Controlator intelligente"

#~ msgid "Satellite communications controller"
#~ msgstr "Controlator de communicationes via satellite"

#~ msgid "Encryption controller"
#~ msgstr "Controlator de cryptation"

#~ msgid "Signal processing controller"
#~ msgstr "Controlator de processar de signal"

#~ msgid "Processing accelerators"
#~ msgstr "Acceleratores de processar"

#~ msgid "Non-Essential Instrumentation"
#~ msgstr "Instrumentation non essential"

#~ msgid "Coprocessor"
#~ msgstr "Co-Processor"

#~ msgid "Unassigned class"
#~ msgstr "Classe non assignate"

#~ msgid "Unknown device class"
#~ msgstr "Classe de dispositivo incognite"

#~ msgid "Non-VGA unclassified device"
#~ msgstr "Dispositivo non classificate non-VGA"

#~ msgid "VGA unclassified device"
#~ msgstr "Dispositivo non classificate VGA"

#~ msgid "Unknown unclassified device"
#~ msgstr "Dispositivo incognite non classificate"

#~ msgid "SCSI storage controller"
#~ msgstr "Controlator de memoria (immagazinage) SCSI"

#~ msgid "IDE controller"
#~ msgstr "Controlator IDE"

#~ msgid "Floppy disk controller"
#~ msgstr "Controlator de disco floppy"

#~ msgid "IPI bus controller"
#~ msgstr "Controlator de bus IPI"

#~ msgid "RAID bus controller"
#~ msgstr "Controlator de bus RAID"

#~ msgid "ATA controller"
#~ msgstr "Controlator ATA"

#~ msgid "SATA controller"
#~ msgstr "Controlator SATA"

#~ msgid "Serial Attached SCSI controller"
#~ msgstr "Controlator SCSI attachate serial"

#~ msgid "Non-Volatile memory controller"
#~ msgstr "Controlator de memoria non volatile"

#~ msgid "Unknown storage controller"
#~ msgstr "Controlator de memoria (immagazinage) incognite"

#~ msgid "Ethernet controller"
#~ msgstr "Controlator Ethernet"

#~ msgid "Token ring network controller"
#~ msgstr "Controlator de rete Token Ring"

#~ msgid "FDDI network controller"
#~ msgstr "Controlator de rete FDDI"

#~ msgid "ATM network controller"
#~ msgstr "Controlator de rete ATM"

#~ msgid "ISDN controller"
#~ msgstr "Controlator ISDN"

#~ msgid "WorldFip controller"
#~ msgstr "Controlator WorldFip"

#~ msgid "PICMG controller"
#~ msgstr "Controlator PICMG"

#~ msgid "Infiniband controller"
#~ msgstr "Controlator de Infiniband"

#~ msgid "Fabric controller"
#~ msgstr "Controlator de structura (Fabric)"

#~ msgid "Unknown network controller"
#~ msgstr "Controlator de rete incognite"

#~ msgid "VGA compatible controller"
#~ msgstr "Controlator compatibile VGA"

#~ msgid "XGA compatible controller"
#~ msgstr "Controlator compatibile XGA"

#~ msgid "3D controller"
#~ msgstr "Controlator 3D"

#~ msgid "Unknown display controller"
#~ msgstr "Controlator de schermo incognite"

#~ msgid "Multimedia video controller"
#~ msgstr "Controlator de video multimedia"

#~ msgid "Multimedia audio controller"
#~ msgstr "Controlator de audio multimedia"

#~ msgid "Computer telephony device"
#~ msgstr "Dispositivo de telephonia de computator"

#~ msgid "Audio device"
#~ msgstr "Dispositivo audio"

#~ msgid "Unknown multimedia controller"
#~ msgstr "Controlator multimedia incognite"

#~ msgid "RAM memory"
#~ msgstr "Memoria RAM"

#~ msgid "FLASH memory"
#~ msgstr "Memoria FLASH"

#~ msgid "Unknown memory controller"
#~ msgstr "Controlator de memoria incognite"

#~ msgid "Host bridge"
#~ msgstr "Ponte hospite"

#~ msgid "ISA bridge"
#~ msgstr "Ponte ISA"

#~ msgid "EISA bridge"
#~ msgstr "Ponte EISA"

#~ msgid "MicroChannel bridge"
#~ msgstr "Ponte MicroChannel"

#~ msgid "PCI bridge"
#~ msgstr "Ponte PCI"

#~ msgid "PCMCIA bridge"
#~ msgstr "Ponte PCMCIA"

#~ msgid "NuBus bridge"
#~ msgstr "Ponte NuBus"

#~ msgid "CardBus bridge"
#~ msgstr "Ponte CardBus"

#~ msgid "RACEway bridge"
#~ msgstr "Ponte RACEway"

#~ msgid "Semi-transparent PCI-to-PCI bridge"
#~ msgstr "Ponte semi-transparent ex PCI-a-PCI"

#~ msgid "InfiniBand to PCI host bridge"
#~ msgstr "Ponte ex InfiniBand a host PCI"

#~ msgid "Unknown bridge"
#~ msgstr "Ponte incognite"

#~ msgid "Serial controller"
#~ msgstr "Controlator serial"

#~ msgid "Parallel controller"
#~ msgstr "Controlator parallel"

#~ msgid "Multiport serial controller"
#~ msgstr "Controlator de serial multiporto"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgid "GPIB controller"
#~ msgstr "Controlator GPIB"

#~ msgid "Smart card controller"
#~ msgstr "Controlator de SmartCard (Carta intelligente)"

#~ msgid "Unknown communication controller"
#~ msgstr "controlator de communication incognite"

#~ msgid "PIC"
#~ msgstr "PIC"

#~ msgid "DMA controller"
#~ msgstr "controlator DMA"

#~ msgid "Timer"
#~ msgstr "Temporisator"

#~ msgid "RTC"
#~ msgstr "RTC"

#~ msgid "PCI Hot-plug controller"
#~ msgstr "controlator de PCI Hot-plug"

#~ msgid "SD Host controller"
#~ msgstr "Controlator de hospite SD"

#~ msgid "IOMMU"
#~ msgstr "IOMMU"

#~ msgid "Unknown system peripheral"
#~ msgstr "systema peripheral incognite"

#~ msgid "Keyboard controller"
#~ msgstr "controlator de claviero"

#~ msgid "Digitizer Pen"
#~ msgstr "Penna Digitizer"

#~ msgid "Mouse controller"
#~ msgstr "Controlator de Mus"

#~ msgid "Scanner controller"
#~ msgstr "controlator de scanditor"

#~ msgid "Gameport controller"
#~ msgstr "controlator de porto de jocos"

#~ msgid "Unknown input device controller"
#~ msgstr "controlator de dispositivo de ingresso incognite"

#~ msgid "Generic docking station"
#~ msgstr "generic station de bassino"

#~ msgid "Unknown docking station"
#~ msgstr "station de bassino incognite"

#~ msgid "386"
#~ msgstr "386"

#~ msgid "486"
#~ msgstr "486"

#~ msgid "Pentium"
#~ msgstr "Pentium"

#~ msgid "Alpha"
#~ msgstr "Alpha"

#~ msgid "Power PC"
#~ msgstr "Power PC"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "Co-processor"
#~ msgstr "Co-Processor"

#~ msgid "Unknown processor"
#~ msgstr "Processor incognite"

#~ msgid "FireWire (IEEE 1394)"
#~ msgstr "FireWire (IEEE 1394)"

#~ msgid "ACCESS bus"
#~ msgstr "bus ACCESS"

#~ msgid "SSA"
#~ msgstr "SSA"

#~ msgid "USB controller"
#~ msgstr "Controlator USB"

#~ msgid "Fibre channel"
#~ msgstr "Canal de Fibras"

#~ msgid "SMBus"
#~ msgstr "SMBus"

#~ msgid "InfiniBand"
#~ msgstr "InfiniBand"

#~ msgid "IPMI interface"
#~ msgstr "interfacie IPMI"

#~ msgid "SERCOS interface"
#~ msgstr "interfacie SERCOS"

#~ msgid "CANbus"
#~ msgstr "CANbus"

#~ msgid "Unknown serial bus controller"
#~ msgstr "controlator de bus serial incognite"

#~ msgid "IRDA controller"
#~ msgstr "controlator IRDA"

#~ msgid "Consumer IR controller"
#~ msgstr "controlator de IR pro consumitor"

#~ msgid "RF controller"
#~ msgstr "controlator RF"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Broadband"
#~ msgstr "Banda Large (Broadband)"

#~ msgid "Ethernet (802.11a - 5 GHz)"
#~ msgstr "Ethernet (802.11a -2.4 GHz)"

#~ msgid "Ethernet (802.11b - 2.4 GHz)"
#~ msgstr "Ethernet (802.11b -2.4 GHz)"

#~ msgid "Unknown wireless controller"
#~ msgstr "controlator wireless incognite"

#~ msgid "I2O"
#~ msgstr "I20"

#~ msgid "Unknown intelligent controller"
#~ msgstr "controlator intelligente incognite"

#~ msgid "Satellite TV controller"
#~ msgstr "controlator de TV de Satellite"

#~ msgid "Satellite audio communication controller"
#~ msgstr "controlator de communication audio de satellite"

#~ msgid "Satellite voice communication controller"
#~ msgstr "controlator de communication de voce de satellite"

#~ msgid "Satellite data communication controller"
#~ msgstr "controlator de communicationes de datos de satellite"

#~ msgid "Unknown satellite communications controller"
#~ msgstr "controlator de communicationes satellite incognite"

#~ msgid "Network and computing encryption device"
#~ msgstr "dispositivo de encryption de computator e rete"

#~ msgid "Entertainment encryption device"
#~ msgstr "dispositivo de encryption de divertimento"

#~ msgid "Unknown encryption controller"
#~ msgstr "controlator de encryption incognite"

#~ msgid "DPIO module"
#~ msgstr "modulo DPIO"

#~ msgid "Performance counters"
#~ msgstr "contatores de prestation"

#~ msgid "Communication synchronizer"
#~ msgstr "Synchronisator de Communication"

#~ msgid "Signal processing management"
#~ msgstr "Gestion de processar de signal"

#~ msgid "Unknown signal processing controller"
#~ msgstr "controlator de processator de signales incognite"

#~ msgid "Unknown processing accelerator"
#~ msgstr "Accelerator de processar incognite"

#~ msgid "Unknown subdevice class"
#~ msgstr "classe de subdispositivos incognite"

#~ msgid "ISA Compatibility mode-only controller"
#~ msgstr "Controlator de sol modo compatibile ISA"

#~ msgid "PCI native mode-only controller"
#~ msgstr "Controlator de solo modo native PCI"

#~ msgid ""
#~ "ISA Compatibility mode controller, supports both channels switched to PCI "
#~ "native mode"
#~ msgstr ""
#~ "Controlator de modo de compatibilitate ISA, supporta ambe canales "
#~ "commutate a modo native PCI"

#~ msgid ""
#~ "PCI native mode controller, supports both channels switched to ISA "
#~ "compatibility mode"
#~ msgstr ""
#~ "Controlator de modo de compatibilitate PCI, supporta ambe canales "
#~ "commutate a modo de compatibilitate ISA"

#~ msgid "ISA Compatibility mode-only controller, supports bus mastering"
#~ msgstr "Controlator de sol modo compatibile ISA, supporta bus mastering"

#~ msgid "PCI native mode-only controller, supports bus mastering"
#~ msgstr "Controlator de sol modo native PCI, supporta bus mastering"

#~ msgid ""
#~ "ISA Compatibility mode controller, supports both channels switched to PCI "
#~ "native mode, supports bus mastering"
#~ msgstr ""
#~ "Controlator de modo de compatibilitate ISA, supporta ambe canales "
#~ "commutate a modo native PCI, supporta bus mastering"

#~ msgid ""
#~ "PCI native mode controller, supports both channels switched to ISA "
#~ "compatibility mode, supports bus mastering"
#~ msgstr ""
#~ "Controlator de modo de compatibilitate PCI, supporta ambe canales "
#~ "commutate a modo de compatibilitate ISA, supporta bus mastering"

#~ msgid "ADMA single stepping"
#~ msgstr "Stepping (vadar) singule ADMA"

#~ msgid "ADMA continuous operation"
#~ msgstr "Operation cntinue ADMA"

#~ msgid "Vendor specific"
#~ msgstr "Specific de venditor"

#~ msgid "AHCI 1.0"
#~ msgstr "AHCI 1.0"

#~ msgid "Serial Storage Bus"
#~ msgstr "Serial Storage Bus (Bus de immagazinage serial)"

#~ msgid "NVMHCI"
#~ msgstr "NVMHCI"

#~ msgid "NVM Express"
#~ msgstr "NVM Express"

#~ msgid "VGA controller"
#~ msgstr "Controlator VGA"

#~ msgid "8514 controller"
#~ msgstr "Controlator 8514"

#~ msgid "Normal decode"
#~ msgstr "De-codification Normal"

#~ msgid "Subtractive decode"
#~ msgstr "De-codification subtrahente"

#~ msgid "Transparent mode"
#~ msgstr "Modo transparente"

#~ msgid "Endpoint mode"
#~ msgstr "modo Endpoint (puncto Final)"

#~ msgid "Primary bus towards host CPU"
#~ msgstr "bus primari verso CPU Host"

#~ msgid "Secondary bus towards host CPU"
#~ msgstr "Bus secundari verso CPU Host"

#~ msgid "8250"
#~ msgstr "8250"

#~ msgid "16450"
#~ msgstr "16450"

#~ msgid "16550"
#~ msgstr "16550"

#~ msgid "16650"
#~ msgstr "16650"

#~ msgid "16750"
#~ msgstr "16750"

#~ msgid "16850"
#~ msgstr "16850"

#~ msgid "16950"
#~ msgstr "16950"

#~ msgid "SPP"
#~ msgstr "SPP"

#~ msgid "BiDir"
#~ msgstr "BiDir"

#~ msgid "ECP"
#~ msgstr "ECP"

#~ msgid "IEEE1284"
#~ msgstr "IEEE1284"

#~ msgid "IEEE1284 Target"
#~ msgstr "IEEE1284 Objectivo"

#~ msgid "Generic"
#~ msgstr "Generic"

#~ msgid "Hayes/16450"
#~ msgstr "Hayes/16450"

#~ msgid "Hayes/16550"
#~ msgstr "Hayes/16550"

#~ msgid "Hayes/16650"
#~ msgstr "Hayes/16650"

#~ msgid "Hayes/16750"
#~ msgstr "Hayes/16750"

#~ msgid "8259"
#~ msgstr "8259"

#~ msgid "ISA PIC"
#~ msgstr "ISA PIC"

#~ msgid "EISA PIC"
#~ msgstr "EISA PIC"

#~ msgid "IO-APIC"
#~ msgstr "IO-APIC"

#~ msgid "IO(X)-APIC"
#~ msgstr "IO(X)-APIC"

#~ msgid "8237"
#~ msgstr "8237"

#~ msgid "ISA DMA"
#~ msgstr "ISA DMA"

#~ msgid "EISA DMA"
#~ msgstr "EISA DMA"

#~ msgid "8254"
#~ msgstr "8254"

#~ msgid "ISA timer"
#~ msgstr "Temporisator ISA"

#~ msgid "EISA timers"
#~ msgstr "Temporisatores EISA"

#~ msgid "HPET"
#~ msgstr "HPET"

#~ msgid "ISA RTC"
#~ msgstr "ISA RTC"

#~ msgid "Extended"
#~ msgstr "Extendite"

#~ msgid "OHCI"
#~ msgstr "OHCI"

#~ msgid "UHCI"
#~ msgstr "UHCI"

#~ msgid "EHCI"
#~ msgstr "EHCI"

#~ msgid "XHCI"
#~ msgstr "XHCI"

#~ msgid "Unspecified"
#~ msgstr "Non specificate"

#~ msgid "USB Device"
#~ msgstr "Dispositivo USB"

#~ msgid "SMIC"
#~ msgstr "SMIC"

#~ msgid "Keyboard controller style"
#~ msgstr "stilo de controlator de claviero"

#~ msgid "Block transfer"
#~ msgstr "transferimento de bloco"

#~ msgid "Vital product data"
#~ msgstr "datos de producto vital"

#~ msgid "Slot identification"
#~ msgstr "identification de slot"

#~ msgid "CompactPCI hot swap"
#~ msgstr "hot swap de PCI"

#~ msgid "HyperTransport"
#~ msgstr "HyperTransport"

#~ msgid "Debug port"
#~ msgstr "porto de cribrar"

#~ msgid "CompactPCI central resource control"
#~ msgstr "Controlo de ressource central CompactPCI"

#~ msgid "PCI hot-plug"
#~ msgstr "PC hot-plug"

#~ msgid "AGP x8"
#~ msgstr "AGP x8"

#~ msgid "Secure device"
#~ msgstr "Dispositivo Secur"

#~ msgid "PCI express"
#~ msgstr "PCI Express"

#~ msgid "MSI-X"
#~ msgstr "MSI-X"

#~ msgid "PCI Advanced Features"
#~ msgstr "Characteristicas avantiate de PCI"

#~ msgid "Fast"
#~ msgstr "Rapide"

#~ msgid "Medium"
#~ msgstr "Medium"

#~ msgid "Slow"
#~ msgstr "Lente"

#~ msgid "32 bit"
#~ msgstr "32 bit"

#~ msgid "Below 1M"
#~ msgstr "Infra 1M"

#~ msgid "64 bit"
#~ msgstr "64 bit"

#~ msgid "Standard"
#~ msgstr "Standard"

#~ msgid "CardBus"
#~ msgstr "CardBus"

#~ msgid "1X"
#~ msgstr "1X"

#~ msgid "2X"
#~ msgstr "2X"

#~ msgid "1X & 2X"
#~ msgstr "1X & 2X"

#~ msgid "4X"
#~ msgstr "4X"

#~ msgid "1X & 4X"
#~ msgstr "1X & 4X"

#~ msgid "2X & 4X"
#~ msgstr "2X & 4X"

#~ msgid "1X & 2X & 4X"
#~ msgstr "1X & 2X & 4X"

#~ msgid "8X"
#~ msgstr "8X"

#~ msgid "4X & 8X"
#~ msgstr "4X & 8X"

#~ msgid "4 ms"
#~ msgstr "4 ms"

#~ msgid "16 ms"
#~ msgstr "16 ms"

#~ msgid "64 ms"
#~ msgstr "64 ms"

#~ msgid "256 ms"
#~ msgstr "256 ms"

#~ msgid "Not needed"
#~ msgstr "Non necessari"

#~ msgid "0 (self powered)"
#~ msgstr "0 (auto alimentate)"

#~ msgid "55 mA"
#~ msgstr "55 mA"

#~ msgid "100 mA"
#~ msgstr "100 mA"

#~ msgid "160 mA"
#~ msgstr "160 mA"

#~ msgid "220 mA"
#~ msgstr "220 mA"

#~ msgid "270 mA"
#~ msgstr "270 mA"

#~ msgid "320 mA"
#~ msgstr "320 MA"

#~ msgid "375 mA"
#~ msgstr "375 mA"

#~ msgid "1 vector"
#~ msgstr "1 vector"

#~ msgid "2 vectors"
#~ msgstr "2 vectores"

#~ msgid "4 vectors"
#~ msgstr "4 vectores"

#~ msgid "8 vectors"
#~ msgstr "8 vectores"

#~ msgid "16 vectors"
#~ msgstr "16 vectores"

#~ msgid "32 vectors"
#~ msgstr "32 vectores"

#~ msgid "Serial ATA direct port access"
#~ msgstr "Accesso de porto directe Serial ATA"

#~ msgid "PICMG 2.14 multi computing"
#~ msgstr "PICMG 2.14 multi computation"

#~ msgid "GPIB (IEEE 488.1/2) controller"
#~ msgstr "controlator de GPIB (IEEE 488.1/2)"

#~ msgid "Smart card"
#~ msgstr "carta intelligente (Smart Card)"

#~ msgid "System peripheral"
#~ msgstr "Systema Peripheric"

#~ msgid "Management card"
#~ msgstr "carta de gestion"

#~ msgid "single DMA"
#~ msgstr "DMA singule"

#~ msgid "chained DMA"
#~ msgstr "DMA incatenate"

#~ msgid "VGA compatible"
#~ msgstr "compatibile VGA"

#~ msgid "8514 compatible"
#~ msgstr "compatibile 8514"

#~ msgid "KDE PCI Information Control Module"
#~ msgstr "Modulo de Controlo de Information PCI de KDE"
