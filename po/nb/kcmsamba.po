# Translation of kcmsamba to Norwegian Bokmål
#
# Hans Petter Bieker <bieker@kde.org>, 1998.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2008, 2010, 2014.
# Nils Kristian Tomren <slx@nilsk.net>, 2005, 2007.
# Bjørn Kvisli <bjorn.kvisli@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcmsamba\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2014-09-04 11:57+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Knut Yrvin"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "knut.yrvin@gmail.com"

#: main.cpp:25
#, kde-format
msgid "kcmsamba"
msgstr "kcmsamba"

#: main.cpp:26
#, kde-format
msgid "Samba Status"
msgstr ""

#: main.cpp:30
#, kde-format
msgid "(c) 2002-2020 KDE Information Control Module Samba Team"
msgstr ""

#: main.cpp:31
#, kde-format
msgid "Michael Glauche"
msgstr "Michael Glauche"

#: main.cpp:32
#, kde-format
msgid "Matthias Hoelzer"
msgstr "Matthias Hoelzer"

#: main.cpp:33
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: main.cpp:34
#, kde-format
msgid "Harald Koschinski"
msgstr " Harald Koschinski"

#: main.cpp:35
#, kde-format
msgid "Wilco Greven"
msgstr "Wilco Greven"

#: main.cpp:36
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:37
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgctxt "@title heading above listview"
msgid "User-Created Shares"
msgstr ""

#: package/contents/ui/main.qml:22
#, kde-format
msgctxt "@title heading above listview"
msgid "Mounted Remote Shares"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no directories shared by users"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no Samba shares mounted on this system"
msgstr ""

#: package/contents/ui/ShareListItem.qml:71
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid "Open folder properties to change share settings"
msgstr ""
