# Translation of kcm_pci to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2010, 2015, 2016, 2019, 2020.
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2009.
# Eirik U. Birkeland <eirbir@gmail.com>, 2009.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcm_pci\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2021-12-15 12:20+0100\n"
"Last-Translator: Oystein Steffensen-Alvaervik <ystein@posteo.net>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eirik U. Birkeland,Karl Ove Hufthammer,Håvard Korsvoll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "eirbir@gmail.com,karl@huftis.org,korsvoll@skulelinux.no"

#: main.cpp:26
#, kde-format
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "PCI-informasjon"
