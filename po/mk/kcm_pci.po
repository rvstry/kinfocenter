# translation of kcm_pci.po to Macedonian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcm_pci\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2010-01-31 17:31+0100\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Божидар Проевски"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bobibobi@freemail.com.mk"

#: main.cpp:26
#, fuzzy, kde-format
#| msgid "PCI-X"
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI-X"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: package/contents/ui/main.qml:14
#, fuzzy
#| msgid "Information"
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "Информациja"

#~ msgid "kcm_pci"
#~ msgstr "kcm_pci"

#, fuzzy
#~| msgid "Device"
#~ msgid "PCI Devices"
#~ msgstr "Уред"

#~ msgid "(c) 2008 Nicolas Ternisien(c) 1998 - 2002 Helge Deller"
#~ msgstr "(c) 2008 Nicolas Ternisien(c) 1998 - 2002 Helge Deller"

#~ msgid "Nicolas Ternisien"
#~ msgstr "Nicolas Ternisien"

#~ msgid "Helge Deller"
#~ msgstr "Helge Deller"

#~ msgid "This list displays PCI information."
#~ msgstr "Оваа листа прикажува информации за периферните уреди"

#~ msgid ""
#~ "This display shows information about your computer's PCI slots and the "
#~ "related connected devices."
#~ msgstr ""
#~ "Оваа листа прикажува информации за PCI-лежиштата од Вашиот компјутер и "
#~ "уредите што се поврзани на нив."

#~ msgid "Device Class"
#~ msgstr "Класа на уред"

#~ msgid "Device Subclass"
#~ msgstr "Подкласа на уред"

#~ msgid "Device Programming Interface"
#~ msgstr "Интерфејс за програмирање на уредот"

#~ msgid "Master IDE Device"
#~ msgstr "Главен IDE-уред"

#~ msgid "Secondary programmable indicator"
#~ msgstr "Секундарен програмабилен индикатор"

#~ msgid "Secondary operating mode"
#~ msgstr "Секундарен режим на работа"

#~ msgid "Primary programmable indicator"
#~ msgstr "Примарен програмабилен индикатор"

#~ msgid "Primary operating mode"
#~ msgstr "Примарен режим на работа"

#~ msgid "Vendor"
#~ msgstr "Производител"

#~ msgid "Device"
#~ msgstr "Уред"

#~ msgid "Subsystem"
#~ msgstr "Подсистем"

#~ msgid " - device:"
#~ msgstr " - уред:"

#~ msgid "Interrupt"
#~ msgstr "Прекин"

#~ msgid "IRQ"
#~ msgstr "IRQ"

#~ msgid "Pin"
#~ msgstr "Ножичка"

#~ msgid "Control"
#~ msgstr "Контрола"

#~ msgid "Response in I/O space"
#~ msgstr "Одговор во В/И-просторот"

#~ msgid "Response in memory space"
#~ msgstr "Одговор во меморискиот простор"

#~ msgid "Bus mastering"
#~ msgstr "Контрола на магистралата"

#~ msgid "Response to special cycles"
#~ msgstr "Одговор на специјални циклуси"

#~ msgid "Memory write and invalidate"
#~ msgstr "Запишување и инвалидација на меморија"

#~ msgid "Parity checking"
#~ msgstr "Проверка на парност"

#~ msgid "System error"
#~ msgstr "Системска грешка"

#~ msgid "Back-to-back writes"
#~ msgstr "Запишувања едно-до-друго"

#~ msgid "Status"
#~ msgstr "Статус"

#~ msgid "Interrupt status"
#~ msgstr "Статус на прекин"

#~ msgid "Capability list"
#~ msgstr "Листа на способности"

#~ msgid "66 MHz PCI 2.1 bus"
#~ msgstr "66 MHz PCI 2.1 магистрала"

#~ msgid "User-definable features"
#~ msgstr "Кориснички дефинирани особини"

#~ msgid "Data parity error"
#~ msgstr "Грешка во парноста на податоците"

#~ msgid "Signaled system error"
#~ msgstr "Сигнализирана системска грешка"

#~ msgid "Parity error"
#~ msgstr "Грешка во парност"

#~ msgid "Latency"
#~ msgstr "Доцнење"

#~ msgid "MIN_GNT"
#~ msgstr "MIN_GNT"

#~ msgid "No major requirements (0x00)"
#~ msgstr "Нема главни побарувања (0x00)"

#~ msgid "MAX_LAT"
#~ msgstr "MAX_LAT"

#~ msgid "Header"
#~ msgstr "Заглавие"

#~ msgid "Type"
#~ msgstr "Тип"

#~ msgid "Multifunctional"
#~ msgstr "Мултифункционален"

#~ msgid "Build-in self test"
#~ msgstr "Вграден самостоен тест"

#~ msgid "BIST Capable"
#~ msgstr "BIST-способен"

#~ msgid "BIST Start"
#~ msgstr "BIST-старт"

#~ msgid "Completion code"
#~ msgstr "Код за довршување"

#~ msgid "Size"
#~ msgstr "Големина"

#~ msgid "Address mappings"
#~ msgstr "Мапирања на адреси"

#~ msgid "Mapping %1"
#~ msgstr "Мапирање %1"

#~ msgid "Space"
#~ msgstr "Простор"

#~ msgid "I/O"
#~ msgstr "В/И"

#~ msgid "Memory"
#~ msgstr "Меморија"

#~ msgid "Address"
#~ msgstr "Адреса"

#, fuzzy
#~| msgid "Unassigned"
#~ msgctxt "unassigned address"
#~ msgid "Unassigned"
#~ msgstr "Недоделено"

#, fuzzy
#~| msgid "Unassigned"
#~ msgctxt "unassigned size"
#~ msgid "Unassigned"
#~ msgstr "Недоделено"

#~ msgid "Bus"
#~ msgstr "Магистрала"

#~ msgid "Primary bus number"
#~ msgstr "Број на примарна магистрала"

#~ msgid "Secondary bus number"
#~ msgstr "Број на секундарна магистрала"

#~ msgid "Secondary latency timer"
#~ msgstr "Секундарен тајмер на доцнење"

#~ msgid "CardBus number"
#~ msgstr "Број на CardBus"

#~ msgid "CardBus latency timer"
#~ msgstr "Тајмер за доцнење на CardBus"

#~ msgid "Secondary status"
#~ msgstr "Секундарен статус"

#~ msgid "I/O behind bridge"
#~ msgstr "В/И зад мостот"

#~ msgid "32-bit"
#~ msgstr "32-битно"

#~ msgid "Base"
#~ msgstr "Основа"

#~ msgid "Limit"
#~ msgstr "Граница"

#~ msgid "Memory behind bridge"
#~ msgstr "Меморија зад мостот"

#~ msgid "64-bit"
#~ msgstr "64-битно"

#~ msgid "Bridge control"
#~ msgstr "Контрола на мостот"

#~ msgid "Secondary parity checking"
#~ msgstr "Секундарна проверка на парност"

#~ msgid "Secondary system error"
#~ msgstr "Секундарна системска грешка"

#~ msgid "Secondary bus reset"
#~ msgstr "Секундарен ресет на магистрала"

#~ msgid "2e10 PCI clocks"
#~ msgstr "2e10 PCI такта"

#~ msgid "2e15 PCI clocks"
#~ msgstr "2e15 PCI такта"

#~ msgid "Expansion ROM"
#~ msgstr "ROM за проширување"

#~ msgid "Memory windows"
#~ msgstr "Мемориски прозорци"

#~ msgid "Window %1"
#~ msgstr "Прозорец %1"

#~ msgid "I/O windows"
#~ msgstr "В/И прозорци"

#~ msgid "16-bit"
#~ msgstr "16-битно"

#~ msgid "16-bit legacy interface ports"
#~ msgstr "16-битни застарени порти за поврзување"

#~ msgid "CardBus control"
#~ msgstr "Контрола на CardBus"

#~ msgid "Interrupts for 16-bit cards"
#~ msgstr "Прекини за 16-битни картички"

#~ msgid "Capabilities"
#~ msgstr "Способности"

#~ msgid "Version"
#~ msgstr "Верзија"

#~ msgid "Clock required for PME generation"
#~ msgstr "Такт потребен за генерирање на PME"

#~ msgid "Device-specific initialization required"
#~ msgstr "Потребна е иницијализација специфична за уредот"

#~ msgid "Maximum auxiliary current required in D3 cold"
#~ msgstr "Максимална додатна струја потребна на D3 ладно"

#~ msgid "D1 support"
#~ msgstr "Поддршка за D1"

#~ msgid "D2 support"
#~ msgstr "Поддршка за D2"

#~ msgid "Power management events"
#~ msgstr "Настани од менаџментот на енергија"

#~ msgid "D0"
#~ msgstr "D0"

#~ msgid "D1"
#~ msgstr "D1"

#~ msgid "D2"
#~ msgstr "D2"

#~ msgid "D3 hot"
#~ msgstr "D3 топло"

#~ msgid "D3 cold"
#~ msgstr "D3 ладно"

#~ msgid "Power state"
#~ msgstr "Состојба на енергија"

#~ msgid "Power management"
#~ msgstr "Менаџмент на енергија"

#~ msgid "Power management status"
#~ msgstr "Статус на менаџмент на енергија"

#~ msgid "Bridge status"
#~ msgstr "Статус на мостот"

#~ msgid "Secondary bus state in D3 hot"
#~ msgstr "Состојба на секундарната магистрала во D3 топло"

#~ msgid "B2"
#~ msgstr "B2"

#~ msgid "B3"
#~ msgstr "B3"

#~ msgid "Data"
#~ msgstr "Податоци"

#~ msgid "Revision"
#~ msgstr "Ревизија"

#~ msgid "Rate"
#~ msgstr "Стапка"

#~ msgid "AGP 3.0 mode"
#~ msgstr "Режим AGP 3.0"

#~ msgid "Fast Writes"
#~ msgstr "Брзи запишувања"

#~ msgid "Address over 4 GiB"
#~ msgstr "Адреса над 4 GiB"

#~ msgid "64-bit GART"
#~ msgstr "64-битен GART"

#~ msgid "Cache Coherency"
#~ msgstr "Кохерентност на кешот"

#~ msgid "Calibrating cycle"
#~ msgstr "Циклус за калибрирање"

#~ msgid "Isochronous transactions"
#~ msgstr "Изохрони трансакции"

#~ msgid "Maximum number of AGP command"
#~ msgstr "Максимален број на AGP-наредба"

#~ msgid "Configuration"
#~ msgstr "Конфигурација"

#~ msgid "AGP"
#~ msgstr "AGP"

#~ msgid "Data address"
#~ msgstr "Адреса на податок"

#~ msgid "Transfer completed"
#~ msgstr "Трансферот е завршен"

#~ msgid "Message control"
#~ msgstr "Контрола на пораки"

#~ msgid "Multiple message capable"
#~ msgstr "Способност за повеќекратни пораки"

#~ msgid "Multiple message enable"
#~ msgstr "Овозможени повеќекратни пораки"

#~ msgid "64-bit address"
#~ msgstr "64-битна адреса"

#~ msgid "Per vector masking"
#~ msgstr "Маскирање по вектор"

#~ msgid "Mask"
#~ msgstr "Маска"

#~ msgid "Pending"
#~ msgstr "Чекање"

#~ msgid "Length"
#~ msgstr "Должина"

#~ msgctxt "no data"
#~ msgid "None"
#~ msgstr "Нема"

#~ msgid "Next"
#~ msgstr "Следно"

#~ msgid "0x00 (None)"
#~ msgstr "0x00 (нема)"

#~ msgid "Root only"
#~ msgstr "Само root"

#~ msgid "Value"
#~ msgstr "Вредност"

#~ msgctxt "state of PCI item"
#~ msgid "Enabled"
#~ msgstr "Овозможено"

#~ msgctxt "state of PCI item"
#~ msgid "Disabled"
#~ msgstr "Оневозможено"

#~ msgctxt "state of PCI item"
#~ msgid "Yes"
#~ msgstr "Да"

#~ msgctxt "state of PCI item"
#~ msgid "No"
#~ msgstr "Не"

#~ msgctxt "state of PCI item"
#~ msgid "Unknown"
#~ msgstr "Непознато"

#~ msgid "Unclassified device"
#~ msgstr "Некласифициран уред"

#~ msgid "Network controller"
#~ msgstr "Мрежен контролер"

#~ msgid "Display controller"
#~ msgstr "Контролер за графика"

#~ msgid "Multimedia controller"
#~ msgstr "Мултимедијален контролер"

#~ msgid "Memory controller"
#~ msgstr "Контролер на меморија"

#~ msgid "Bridge"
#~ msgstr "Мост"

#~ msgid "Communication controller"
#~ msgstr "Контролер за комуникација"

#~ msgid "Generic system peripheral"
#~ msgstr "Општ системски периферен уред"

#~ msgid "Input device controller"
#~ msgstr "Контролер за влезен уред"

#~ msgid "Processor"
#~ msgstr "Процесор"

#~ msgid "Serial bus controller"
#~ msgstr "Контролер на сериска магистрала"

#~ msgid "Wireless controller"
#~ msgstr "Контролер за бежична мрежа"

#~ msgid "Intelligent controller"
#~ msgstr "Интелигентен контролер"

#~ msgid "Satellite communications controller"
#~ msgstr "Контролер за сателитска комуникација"

#~ msgid "Encryption controller"
#~ msgstr "Контролер за криптирање"

#~ msgid "Signal processing controller"
#~ msgstr "Контролер за обработка на сигнали"

#, fuzzy
#~| msgid "Co-processor"
#~ msgid "Coprocessor"
#~ msgstr "Копроцесор"

#, fuzzy
#~| msgid "Unassigned"
#~ msgid "Unassigned class"
#~ msgstr "Недоделено"

#~ msgid "Unknown device class"
#~ msgstr "Непозната класа на уред"

#~ msgid "Non-VGA unclassified device"
#~ msgstr "Некласифициран уред, не е VGA"

#~ msgid "VGA unclassified device"
#~ msgstr "Некласифициран уред, VGA"

#~ msgid "Unknown unclassified device"
#~ msgstr "Непознат некласифициран уред"

#~ msgid "IDE controller"
#~ msgstr "IDE-контролер"

#~ msgid "Floppy disk controller"
#~ msgstr "Контролер за дискетна единица"

#~ msgid "IPI bus controller"
#~ msgstr "Контролер на IPI-магистрала"

#~ msgid "RAID bus controller"
#~ msgstr "Контролер на RAID-магистрала"

#~ msgid "ATA controller"
#~ msgstr "ATA-контролер"

#, fuzzy
#~| msgid "ATA controller"
#~ msgid "SATA controller"
#~ msgstr "ATA-контролер"

#, fuzzy
#~| msgid "Serial controller"
#~ msgid "Serial Attached SCSI controller"
#~ msgstr "Контролер за сериски порти"

#, fuzzy
#~| msgid "Unknown memory controller"
#~ msgid "Non-Volatile memory controller"
#~ msgstr "Непознат контролер за меморија"

#~ msgid "Ethernet controller"
#~ msgstr "Етернет-контролер"

#~ msgid "Token ring network controller"
#~ msgstr "Контролер за мрежа „Token ring“"

#~ msgid "FDDI network controller"
#~ msgstr "Контролер за FDDI-мрежа"

#~ msgid "ATM network controller"
#~ msgstr "Контролер за ATM-мрежа"

#~ msgid "ISDN controller"
#~ msgstr "ISDN-контролер"

#, fuzzy
#~| msgid "IDE controller"
#~ msgid "PICMG controller"
#~ msgstr "IDE-контролер"

#, fuzzy
#~| msgid "Keyboard controller"
#~ msgid "Infiniband controller"
#~ msgstr "Контролер за тастатура"

#, fuzzy
#~| msgid "RF controller"
#~ msgid "Fabric controller"
#~ msgstr "RF-контролер"

#~ msgid "Unknown network controller"
#~ msgstr "Непознат контролер за мрежа"

#~ msgid "VGA compatible controller"
#~ msgstr "Контролер компатибилен со VGA"

#~ msgid "XGA compatible controller"
#~ msgstr "Контролер компатибилен со XGA"

#~ msgid "3D controller"
#~ msgstr "Контролер за 3D"

#~ msgid "Unknown display controller"
#~ msgstr "Непознат контролер за графика"

#~ msgid "Multimedia video controller"
#~ msgstr "Контролер за мултимедијално видео"

#~ msgid "Multimedia audio controller"
#~ msgstr "Контролер за мултимедијално аудио"

#~ msgid "Computer telephony device"
#~ msgstr "Уред за компјутерска телефонија"

#, fuzzy
#~| msgid "Secure device"
#~ msgid "Audio device"
#~ msgstr "Безбеден уред"

#~ msgid "Unknown multimedia controller"
#~ msgstr "Непознат мултимедијален контролер"

#~ msgid "RAM memory"
#~ msgstr "RAM-меморија"

#~ msgid "FLASH memory"
#~ msgstr "FLASH-меморија"

#~ msgid "Unknown memory controller"
#~ msgstr "Непознат контролер за меморија"

#~ msgid "ISA bridge"
#~ msgstr "ISA-мост"

#~ msgid "EISA bridge"
#~ msgstr "EISA-мост"

#~ msgid "MicroChannel bridge"
#~ msgstr "MicroChannel-мост"

#~ msgid "PCI bridge"
#~ msgstr "PCI-мост"

#~ msgid "PCMCIA bridge"
#~ msgstr "PCMCIA-мост"

#~ msgid "NuBus bridge"
#~ msgstr "NuBus-мост"

#~ msgid "CardBus bridge"
#~ msgstr "CardBus-мост"

#~ msgid "RACEway bridge"
#~ msgstr "RACEway-мост"

#~ msgid "Unknown bridge"
#~ msgstr "Непознат мост"

#~ msgid "Serial controller"
#~ msgstr "Контролер за сериски порти"

#~ msgid "Parallel controller"
#~ msgstr "Контролер за паралелни порти"

#~ msgid "Multiport serial controller"
#~ msgstr "Контролер за мултипортни сериски уреди"

#~ msgid "Modem"
#~ msgstr "Модем"

#, fuzzy
#~| msgid "USB controller"
#~ msgid "GPIB controller"
#~ msgstr "USB-контролер"

#, fuzzy
#~| msgid "Scanner controller"
#~ msgid "Smart card controller"
#~ msgstr "Контролер за скенер"

#~ msgid "Unknown communication controller"
#~ msgstr "Непознат контролер за комуникација"

#~ msgid "PIC"
#~ msgstr "PIC"

#~ msgid "DMA controller"
#~ msgstr "DMA-контролер"

#~ msgid "Timer"
#~ msgstr "Тајмер"

#~ msgid "RTC"
#~ msgstr "RTC"

#, fuzzy
#~| msgid "ISDN controller"
#~ msgid "SD Host controller"
#~ msgstr "ISDN-контролер"

#~ msgid "Unknown system peripheral"
#~ msgstr "Непознат системски периферен уред"

#~ msgid "Keyboard controller"
#~ msgstr "Контролер за тастатура"

#~ msgid "Mouse controller"
#~ msgstr "Контролер за глушец"

#~ msgid "Scanner controller"
#~ msgstr "Контролер за скенер"

#~ msgid "Gameport controller"
#~ msgstr "Контролер за џојстик"

#~ msgid "Unknown input device controller"
#~ msgstr "Непознат контролер за влезен уред"

#~ msgid "386"
#~ msgstr "386"

#~ msgid "486"
#~ msgstr "486"

#~ msgid "Pentium"
#~ msgstr "Pentium"

#~ msgid "Alpha"
#~ msgstr "Alpha"

#~ msgid "Power PC"
#~ msgstr "Power PC"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "Co-processor"
#~ msgstr "Копроцесор"

#~ msgid "Unknown processor"
#~ msgstr "Непознат процесор"

#~ msgid "FireWire (IEEE 1394)"
#~ msgstr "FireWire (IEEE 1394)"

#~ msgid "ACCESS bus"
#~ msgstr "ACCESS-магистрала"

#~ msgid "SSA"
#~ msgstr "SSA"

#~ msgid "USB controller"
#~ msgstr "USB-контролер"

#~ msgid "SMBus"
#~ msgstr "SMBus"

#~ msgid "InfiniBand"
#~ msgstr "InfiniBand"

#~ msgid "IPMI interface"
#~ msgstr "IPMI-интерфејс"

#~ msgid "SERCOS interface"
#~ msgstr "SERCOS-интерфејс"

#~ msgid "CANbus"
#~ msgstr "CANbus"

#~ msgid "Unknown serial bus controller"
#~ msgstr "Непознат контролер за сериска магистрала"

#~ msgid "IRDA controller"
#~ msgstr "IRDA-контролер"

#~ msgid "RF controller"
#~ msgstr "RF-контролер"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Broadband"
#~ msgstr "Широкопојасен"

#~ msgid "Ethernet (802.11a - 5 GHz)"
#~ msgstr "Етернет (802.11a - 5 GHz)"

#~ msgid "Ethernet (802.11b - 2.4 GHz)"
#~ msgstr "Етернет (802.11b - 2.4 GHz)"

#~ msgid "Unknown wireless controller"
#~ msgstr "Непознат контролер за бежична мрежа"

#~ msgid "I2O"
#~ msgstr "I2O"

#~ msgid "Unknown intelligent controller"
#~ msgstr "Непознат интелигентен контролер"

#~ msgid "Satellite TV controller"
#~ msgstr "Контролер за сателитска телевизија"

#~ msgid "Satellite audio communication controller"
#~ msgstr "Контролер за сателитска аудиокомуникација"

#~ msgid "Satellite voice communication controller"
#~ msgstr "Контролер за сателитска гласовна комуникација"

#~ msgid "Satellite data communication controller"
#~ msgstr "Контролер за сателитска податочна комуникација"

#~ msgid "Unknown satellite communications controller"
#~ msgstr "Непознат контролер за сателитска комуникација"

#~ msgid "Network and computing encryption device"
#~ msgstr "Уред за мрежа и криптографски пресметки"

#~ msgid "Unknown encryption controller"
#~ msgstr "Непознат криптографски контролер"

#~ msgid "DPIO module"
#~ msgstr "DPIO-модул"

#~ msgid "Performance counters"
#~ msgstr "Бројачи на перформанси"

#~ msgid "Communication synchronizer"
#~ msgstr "Синхронизатор на комуникација"

#, fuzzy
#~| msgid "Signal processing controller"
#~ msgid "Signal processing management"
#~ msgstr "Контролер за обработка на сигнали"

#~ msgid "Unknown signal processing controller"
#~ msgstr "Непознат контролер за обработка на сигнали"

#, fuzzy
#~| msgid "Unknown processor"
#~ msgid "Unknown processing accelerator"
#~ msgstr "Непознат процесор"

#, fuzzy
#~| msgid "VGA compatible controller"
#~ msgid "ISA Compatibility mode-only controller"
#~ msgstr "Контролер компатибилен со VGA"

#, fuzzy
#~| msgid "Communication controller"
#~ msgid "PCI native mode-only controller"
#~ msgstr "Контролер за комуникација"

#~ msgid "Vendor specific"
#~ msgstr "Специфично за производителот"

#, fuzzy
#~| msgid "PCI express"
#~ msgid "NVM Express"
#~ msgstr "PCI express"

#, fuzzy
#~| msgid "ATA controller"
#~ msgid "VGA controller"
#~ msgstr "ATA-контролер"

#, fuzzy
#~| msgid "3D controller"
#~ msgid "8514 controller"
#~ msgstr "Контролер за 3D"

#~ msgid "8250"
#~ msgstr "8250"

#~ msgid "16450"
#~ msgstr "16450"

#~ msgid "16550"
#~ msgstr "16550"

#~ msgid "16650"
#~ msgstr "16650"

#~ msgid "16750"
#~ msgstr "16750"

#~ msgid "16850"
#~ msgstr "16850"

#~ msgid "16950"
#~ msgstr "16950"

#~ msgid "SPP"
#~ msgstr "SPP"

#~ msgid "BiDir"
#~ msgstr "BiDir"

#~ msgid "ECP"
#~ msgstr "ECP"

#~ msgid "IEEE1284"
#~ msgstr "IEEE1284"

#~ msgid "Generic"
#~ msgstr "Општо"

#~ msgid "Hayes/16450"
#~ msgstr "Hayes/16450"

#~ msgid "Hayes/16550"
#~ msgstr "Hayes/16550"

#~ msgid "Hayes/16650"
#~ msgstr "Hayes/16650"

#~ msgid "Hayes/16750"
#~ msgstr "Hayes/16750"

#~ msgid "8259"
#~ msgstr "8259"

#~ msgid "ISA PIC"
#~ msgstr "ISA PIC"

#~ msgid "EISA PIC"
#~ msgstr "EISA PIC"

#~ msgid "IO-APIC"
#~ msgstr "IO-APIC"

#~ msgid "IO(X)-APIC"
#~ msgstr "IO(X)-APIC"

#~ msgid "8237"
#~ msgstr "8237"

#~ msgid "ISA DMA"
#~ msgstr "ISA DMA"

#~ msgid "EISA DMA"
#~ msgstr "EISA DMA"

#~ msgid "8254"
#~ msgstr "8254"

#~ msgid "ISA timer"
#~ msgstr "ISA-тајмер"

#~ msgid "EISA timers"
#~ msgstr "EISA-тајмери"

#~ msgid "ISA RTC"
#~ msgstr "ISA RTC"

#~ msgid "Extended"
#~ msgstr "Проширено"

#~ msgid "OHCI"
#~ msgstr "OHCI"

#~ msgid "UHCI"
#~ msgstr "UHCI"

#~ msgid "EHCI"
#~ msgstr "EHCI"

#~ msgid "Unspecified"
#~ msgstr "Неодредено"

#~ msgid "USB Device"
#~ msgstr "USB-уред"

#~ msgid "SMIC"
#~ msgstr "SMIC"

#~ msgid "Keyboard controller style"
#~ msgstr "Стил на контролер на тастатура"

#~ msgid "Block transfer"
#~ msgstr "Блоковски пренос"

#~ msgid "Slot identification"
#~ msgstr "Идентификација на лежиште"

#~ msgid "HyperTransport"
#~ msgstr "HyperTransport"

#~ msgid "AGP x8"
#~ msgstr "AGP x8"

#~ msgid "Secure device"
#~ msgstr "Безбеден уред"

#~ msgid "PCI express"
#~ msgstr "PCI express"

#~ msgid "MSI-X"
#~ msgstr "MSI-X"

#~ msgid "Fast"
#~ msgstr "Брзо"

#~ msgid "Medium"
#~ msgstr "Средно"

#~ msgid "Slow"
#~ msgstr "Бавно"

#~ msgid "32 bit"
#~ msgstr "32 бита"

#~ msgid "Below 1M"
#~ msgstr "Под 1M"

#~ msgid "64 bit"
#~ msgstr "64 битно"

#~ msgid "Standard"
#~ msgstr "Стандардно"

#~ msgid "CardBus"
#~ msgstr "CardBus"

#~ msgid "1X"
#~ msgstr "1X"

#~ msgid "2X"
#~ msgstr "2X"

#~ msgid "1X & 2X"
#~ msgstr "1X и 2X"

#~ msgid "4X"
#~ msgstr "4X"

#~ msgid "1X & 4X"
#~ msgstr "1X и 4X"

#~ msgid "2X & 4X"
#~ msgstr "2X и 4X"

#~ msgid "1X & 2X & 4X"
#~ msgstr "1X и 2X и 4X"

#~ msgid "8X"
#~ msgstr "8X"

#~ msgid "4X & 8X"
#~ msgstr "4X и 8X"

#~ msgid "4 ms"
#~ msgstr "4 ms"

#~ msgid "16 ms"
#~ msgstr "16 ms"

#~ msgid "64 ms"
#~ msgstr "64 ms"

#~ msgid "256 ms"
#~ msgstr "256 ms"

#~ msgid "Not needed"
#~ msgstr "Не е потребно"

#~ msgid "0 (self powered)"
#~ msgstr "0 (само се напојува)"

#~ msgid "55 mA"
#~ msgstr "55 mA"

#~ msgid "100 mA"
#~ msgstr "100 mA"

#~ msgid "160 mA"
#~ msgstr "160 mA"

#~ msgid "220 mA"
#~ msgstr "220 mA"

#~ msgid "270 mA"
#~ msgstr "270 mA"

#~ msgid "320 mA"
#~ msgstr "320 mA"

#~ msgid "375 mA"
#~ msgstr "375 mA"

#~ msgid "1 vector"
#~ msgstr "1 вектор"

#~ msgid "2 vectors"
#~ msgstr "2 вектори"

#~ msgid "4 vectors"
#~ msgstr "4 вектори"

#~ msgid "8 vectors"
#~ msgstr "8 вектори"

#~ msgid "16 vectors"
#~ msgstr "16 вектори"

#~ msgid "32 vectors"
#~ msgstr "32 вектори"

#~ msgid "Serial ATA direct port access"
#~ msgstr "Директен пристап до порта на сериска ATA"

#~ msgid "GPIB (IEEE 488.1/2) controller"
#~ msgstr "GPIB (IEEE 488.1/2) контролер"

#~ msgid "Smart card"
#~ msgstr "Паметна картичка"

#~ msgid "System peripheral"
#~ msgstr "Системски периферен уред"

#~ msgid "single DMA"
#~ msgstr "единечен DMA"

#~ msgid "VGA compatible"
#~ msgstr "Компатибилно со VGA"

#~ msgid "8514 compatible"
#~ msgstr "Компатибилно со 8514"

#~ msgid "KDE PCI Information Control Module"
#~ msgstr "Контролен модул на KDE за информации за периферни уреди"
