# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-19 00:46+0000\n"
"PO-Revision-Date: 2022-09-18 12:16+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: main.cpp:28
#, kde-format
msgctxt "@label kcm name"
msgid "Window Manager"
msgstr "Window Manager"

#: main.cpp:32
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: package/contents/ui/main.qml:15
msgctxt "@info"
msgid "KWin Support Information"
msgstr "KWin Support Information"
